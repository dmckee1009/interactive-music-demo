Event	ID	Name			Wwise Object Path	Notes
	417627684	playmusic			\Start\playmusic	
	1067713692	beatSync			\system\beatSync	
	3086540886	stopAll			\system\stopAll	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	36929138	start loop	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\start loop_F26CE7B0.wem		\Interactive Music Hierarchy\start\music sections\loop\loop\start loop		2185292
	258364925	start-alt main loop 140 BPM	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\start-alt main loop 140 BPM_F26CE7B0.wem		\Interactive Music Hierarchy\start-alt\start-alt\Start-alt loop\start-alt main loop 140 BPM\start-alt main loop 140 BPM		6330052
	310638426	Start intro	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\Start intro_F26CE7B0.wem		\Interactive Music Hierarchy\start\music sections\intro\intro\Start intro		17848
	427052515	start outro	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\start outro_F26CE7B0.wem		\Interactive Music Hierarchy\start\music sections\outro\outro\start outro		17768
	558190116	start-alt end	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\start-alt end_F26CE7B0.wem		\Interactive Music Hierarchy\start-alt\start-alt\Start-alt outro\start-alt end\start-alt end		2568952
	607441052	Start-alt intro 140BPM	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\Start-alt intro 140BPM_F26CE7B0.wem		\Interactive Music Hierarchy\start-alt\start-alt\Start-alt intro\Start-alt intro 140BPM\Start-alt intro 140BPM		6336352

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	310638426	Start intro	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\Start intro_F26CE7B0.wem	310638426.wem	\Interactive Music Hierarchy\start\music sections\intro\intro\Start intro	
	427052515	start outro	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Windows\SFX\start outro_F26CE7B0.wem	427052515.wem	\Interactive Music Hierarchy\start\music sections\outro\outro\start outro	

