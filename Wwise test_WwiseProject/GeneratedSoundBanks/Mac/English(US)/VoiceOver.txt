Event	ID	Name			Wwise Object Path	Notes
	26822469	exit			\VoiceOver\exit	
	146821428	bgStart			\futurebass\bgStart	
	401202141	gChordOff			\futurebass\gChordOff	
	629923840	area2			\VoiceOver\area2	
	629923843	area1			\VoiceOver\area1	
	652878897	drKick			\futurebass\drKick	
	746480971	drShaker			\futurebass\drShaker	
	902446744	bgStart_alt			\futurebass\bgStart_alt	
	953277036	resume			\VoiceOver\resume	
	1203400786	restart			\VoiceOver\restart	
	1203933600	titleTIP			\VoiceOver\titleTIP	
	1442411811	drShaker2			\futurebass\drShaker2	
	1796835497	intenseInfo			\VoiceOver\intenseInfo	
	1865857008	transition			\futurebass\transition	
	2048836982	drSnare			\futurebass\drSnare	
	2245437400	gChord2			\futurebass\gChord2	
	2245437401	gChord3			\futurebass\gChord3	
	2245437403	gChord1			\futurebass\gChord1	
	2245437406	gChord4			\futurebass\gChord4	
	2341146698	FutureBInfo			\VoiceOver\FutureBInfo	
	2427688735	areaOffline			\VoiceOver\areaOffline	
	2509093697	stopAudio			\VoiceOver\stopAudio	
	2515370705	Play_A2_Instructions			\Default Work Unit\Play_A2_Instructions	
	2609767683	bgdrums			\futurebass\bgdrums	
	2614957251	chord4			\futurebass\chord4	
	2614957252	chord3			\futurebass\chord3	
	2614957253	chord2			\futurebass\chord2	
	2614957254	chord1			\futurebass\chord1	
	3092587493	pause			\VoiceOver\pause	
	3705726509	title			\VoiceOver\title	
	3814687627	bgDrums2			\futurebass\bgDrums2	
	3903699212	chordOff			\futurebass\chordOff	
	4072995806	areofflineX			\VoiceOver\areofflineX	

State Group	ID	Name			Wwise Object Path	Notes
	2609767683	bgDrums			\Area 2 Futurebass\bgDrums	

State	ID	Name	State Group			Notes
	0	None	bgDrums			
	2149848048	buildup	bgDrums			
	3177314144	part2	bgDrums			
	3177314147	part1	bgDrums			
	4163340556	patterns	bgDrums			

Modulator Envelope	ID	Name			Wwise Object Path	Notes
	69409206	Modulator Envelope (Custom)				
	563507829	Modulator Envelope (Custom)				

Trigger	ID	Name			Wwise Object Path	Notes
	652878897	drKick			\Futurebass\drKick	
	746480971	drShaker			\Futurebass\drShaker	
	1442411811	drShaker2			\Futurebass\drShaker2	
	2048836982	drSnare			\Futurebass\drSnare	
	2609767683	bgDrums			\Futurebass\bgDrums	
	2614957251	chord4			\Futurebass\chord4	
	2614957252	chord3			\Futurebass\chord3	
	2614957253	chord2			\Futurebass\chord2	
	2614957254	chord1			\Futurebass\chord1	

Audio Bus	ID	Name			Wwise Object Path	Notes
	3170124113	Voice			\Default Work Unit\Master Audio Bus\Voice	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Effect plug-ins	ID	Name	Type				Notes
	294961413	Wwise RoomVerb (Custom)	Wwise RoomVerb			
	318936652	Blue_Distant_Echoes (Custom)	Wwise Stereo Delay			
	911266962	Wwise Meter (Custom)	Wwise Meter			
	2653529179	Three_Voices	Wwise Harmonizer			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	1706740	Line8v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line8v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Area offline\Line8v2		250624
	2607427	L4 (The face buttons)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L4 (The face buttons)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L4 (The face buttons)		534228
	16145944	Region  2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 10_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord2\Region  2		247336
	20624527	Line26P2v3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line26P2v3_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line26(Thanks)\Line26P2\Line26P2v3		145500
	21247141	Line16v1(restart)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line16v1(restart)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\restart\Line16v1(restart)		124460
	26558495	Region  2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 2_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord2\Region  2		282988
	28804660	Line7P1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line7P1_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line7\Line7P1\Line7P1		264944
	29787732	Line 1-MOD	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line 1-MOD_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line 1-MOD		1720256
	35242678	Region  2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 14_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord2\Region  2		247280
	54325753	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drFX2_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker2\Shaker		70688
	59829682	melC4F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC4S_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord4\melC4F\melC4F		274
	79935900	bg4B	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\bg4B_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgArps\bg4B\bg4B		282368
	81534158	melC1F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC1S_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord1\melC1F\melC1F		324
	81925663	bChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\bChord4_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bChords\bChord1\bChord\bChord1		308176
	86588433	Kick	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Kick_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drKick\Kick		45296
	92081348	L7 (L2 will go back)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L7 (L2 will go back)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L7 (L2 will go back)		381360
	96349398	Melody_note	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Melody_note_0F9E1E23.wem		\Actor-Mixer Hierarchy\Futurebass\Melody_blendnote\Melody_note		1626608
	102387122	melC2F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC2S_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord2\melC2F\melC2F		287
	108879139	Region  3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 7_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord3\Region  3		282332
	131274040	L3 (Directional buttons)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L3 (Directional buttons)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L3 (Directional buttons)		420812
	141030427	bgShakers	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\bgShakers_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\bgDrums\bgDrums\bgShakers		2457808
	147337574	Line15v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line15v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\resume\Line15v2		133988
	175946376	gChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\gChord3_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\gChords_alt\gChord1\gChords\gChord1		282368
	232713497	drLongBuild	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drLongBuild_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bChords\bChord1\bChord\drLongBuild		2466528
	237506644	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Shaker_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker\Shaker		44316
	245217840	ESWFB Hip Hop Snare 06	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\ESWFB Hip Hop Snare 06_6A598099.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\ESWFB Hip Hop Snare 06		100136
	271865377	drClap2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drClap2_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\drClap2		160724
	271884298	Beat 1+Hats	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Beat 1+Hats_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\Rhythms\Beat\Beat 1+Hats		1271
	287299100	drCrash	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drCrash_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\drCrash		300052
	288217393	bg3A	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\bg3A_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgArps\bg3A\bg3A\bg3A		282368
	304253256	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\fx2_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker\Shaker		70688
	304667256	Region  1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 1_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord1_01\Region  1		281644
	306876623	Beat 1+Hats	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Hats_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\Rhythms\Beat\Beat 1+Hats		1225
	310490421	melC2F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC2M_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord2\melC2F\melC2F		403
	327928273	Line15v1(resume)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line15v1(resume)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\resume\Line15v1(resume)		144176
	331599978	Line3-MOD	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line3-MOD_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line3-MOD		323352
	343760199	melC1F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC1F_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord1\melC1F\melC1F		714
	347297929	Region  2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 6_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord2\Region  2		282444
	348225684	Dreamcore Studios	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Dreamcore Studios_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Dreamcore Studios		258156
	379822989	melC3F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC3S_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord3\melC3F\melC3F		355
	407833491	Melody_note_high_4	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Melody_note_high_4_2010FD4B.wem		\Actor-Mixer Hierarchy\Futurebass\Melody_blendnote\Melody_note_high_4		694620
	423297528	melC4F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC4M_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord4\melC4F\melC4F		401
	439692401	Menu6B	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Menu6B_7544BA96.wem		\Actor-Mixer Hierarchy\UISounds\Menu6B		279992
	441854348	ESWFB Snare 17	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\ESWFB Snare 17_AB48167B.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\ESWFB Snare 17		122036
	446230409	Region  3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 15_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord3\Region  3		282216
	455259918	Line8v1 (offline)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line8v1 (offline)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Area offline\Line8v1 (offline)		462400
	475810115	Line26P2v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line26P2v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line26(Thanks)\Line26P2\Line26P2v2		131848
	531582573	L9 (Press Touch Pad)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L9 (Press Touch Pad)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L9 (Press Touch Pad)		457008
	533742384	Clap	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\ESWFB Snare 17_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drClap\Clap		275988
	537072125	Beat 1+Hats	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\Beat1NoHats_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\Rhythms\Beat\Beat 1+Hats		573
	539937168	drFX1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drFX1_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\drFX1		70688
	540184999	Region  3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 3_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord3\Region  3		281752
	550918960	Line19-25(credits)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line19-25(credits)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line19-25(credits)		4076168
	553905031	melC3F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC3M_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord3\melC3F\melC3F		424
	555028405	L2 (Use the controller)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L2 (Use the controller)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L2 (Use the controller)		534212
	558237119	L6 (R2 will begin)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L6 (R2 will begin)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L6 (R2 will begin)		608192
	572924310	Line16v2(restart)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line16v2(restart)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\restart\Line16v2(restart)		114312
	573758267	Line8v3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line8v3_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Area offline\Line8v3		320432
	578687964	Region  3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 11_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord3\Region  3		282456
	587591439	L5 (Press left stick in)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L5 (Press left stick in)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L5 (Press left stick in)		1515632
	650048620	Line7P1v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line7P1v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line7\Line7P1\Line7P1v2		167632
	650719566	Line26P1v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line26P1v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line26(Thanks)\Line26P1(thanks)\Line26P1v2		243864
	655020996	Line6(area1)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line6_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line6(area1)		1058464
	672223269	bgDrums	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\bgDrums_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\bgDrums\bgDrums\bgDrums		1129184
	693790387	Line14v1(pause)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line14v1(pause)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\paused\Line14v1(pause)		111112
	702641741	melC2F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC2F_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord2\melC2F\melC2F		834
	704623033	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\fx1_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker\Shaker		70688
	722241768	drSnare3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drSnare3_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\drSnare3		226684
	729792837	L8 (Press the Options)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L8 (Press the Options)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\A2 Instructions\L8 (Press the Options)		419272
	730997375	gChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\gChord2_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\gChords_alt\gChord1\gChords\gChord1		282368
	740882907	Line26P1v1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line26P1v1_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line26(Thanks)\Line26P1(thanks)\Line26P1v1		187384
	749770850	gChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\gChord4_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\gChords_alt\gChord1\gChords\gChord1		282368
	766532383	bg2A	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\bg2A_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgArps\bg2A\bg2A\bg2A		282368
	771450126	bChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\bChord3_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bChords\bChord1\bChord\bChord1		308192
	798172600	melC3F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC3F_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord3\melC3F\melC3F		818
	802901964	drBuild - Copy	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drBuild - Copy_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\drBuild\drBuild\drBuild - Copy		564608
	805420424	Line8v4X	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line8v4X_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line8v4X		397432
	834222079	Line4(title	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line4_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line4(title		529264
	835778704	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drFX1_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker2\Shaker		70688
	842730976	Beat 1+Hats	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drSnareClapCombo_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgDrums\Rhythms\Beat\Beat 1+Hats		308448
	850394432	Line11-12 (Hold R2)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line11-12 (Hold R2)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line11-12 (Hold R2)		2029664
	864707073	Line14v2(pause)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line14v2(pause)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\paused\Line14v2(pause)		187916
	879578104	Line2-MOD	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line2-MOD_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line2-MOD		291404
	891102450	Region  4	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Region 16_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Chords\Chord4\Region  4		282324
	906153064	Line13 (PauseExplain)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line13 (PauseExplain)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line13 (PauseExplain)		991056
	906963704	Line17v1(exit)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line17v1(exit)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\exit\Line17v1(exit)		70624
	907934425	Line18(return)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line18(return)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line18(return)		943752
	914339695	Line7P2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line7P2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line7\Line7P2		212440
	944596052	Line26P2v1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line26P2v1_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line26(Thanks)\Line26P2\Line26P2v1		90960
	944923677	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drFX3_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker2\Shaker		70688
	945752516	L1 (Area 2)	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\L1 (Area 2)_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\Eng_Area2\L1 (Area 2)		647612
	960646296	Line17v2	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Line17v2_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\exit\Line17v2		100200
	975564428	melC1F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC1M_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord1\melC1F\melC1F		410
	989085177	Clap	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\Clap_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drClap\Clap		19552
	996140098	drKick3	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\drKick3_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\drKick3		74552
	1003755517	melC4F	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\melC4F_00000000.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Melody Switch\melChord4\melC4F\melC4F		802
	1004232854	Shaker	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\fx3_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\Drums\drShaker\Shaker		70688
	1010190867	bChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\bChord1_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bChords\bChord1\bChord\bChord1		308192
	1017830310	Line5	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\AI Speaker 3_7544BA96.wem		\Actor-Mixer Hierarchy\VoiceOver\Default\English\Line5		1323064
	1023746723	bg1AB	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\Voices\English(US)\bg1AB_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bgArps\bgAB1\bg1AB\bg1AB		282368
	1030892181	ESWFB Kick 02	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\ESWFB Kick 02_F8B8101E.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\ESWFB Kick 02		42000
	1032202180	gChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\gChord1_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\gChords_alt\gChord1\gChords\gChord1		282368
	1039154404	bChord1	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\bChord2_F26CE7B0.wem		\Interactive Music Hierarchy\Futurebass\fullSong\bChords\bChord1\bChord\bChord1		308192
	1050102837	dr - Hihat	G:\Unity Projects\Unity 2018\Dynamic Dream (GIT)\interactive-music-demo\Wwise test_WwiseProject\.cache\Mac\SFX\dr - Hihat_7544BA96.wem		\Actor-Mixer Hierarchy\Futurebass\Drumkit\dr - Hihat		6152

