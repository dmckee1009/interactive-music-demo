﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker.Ecosystem.Utils;

public class PlayMakerFloatVariableProxy : MonoBehaviour {

	public float FloatValue;

	public PlayMakerFsmVariableTarget FsmVariableTarget;

	[FsmVariableTargetVariable("FsmVariableTarget")]
	public PlayMakerFsmVariable FsmVariable;

	// Use this for initialization
	void Start () {
		FsmVariable.GetVariable (FsmVariableTarget);
	}
	
	// Update is called once per frame
	void Update () {

		if (FsmVariable.initialized) {
			FsmVariable.FsmFloat.Value = FloatValue;
		}
	}
}
