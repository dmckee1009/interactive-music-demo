//{"Values":["0","NTEC/IndustrialCamera","_MainTex","0",""]}|{"position":{"serializedVersion":"2","x":0.0,"y":0.0,"width":212.0,"height":109.0},"name":"FloatSlider","selected":false,"Values":["Lines","Lines visibility","0","0","1"],"serial":0,"unique":-1,"type":"FloatSliderField"}|{"tempTextures":1,"passes":[{"position":{"serializedVersion":"2","x":0.0,"y":36.0,"width":212.0,"height":16.0},"InputLabels":["Game","tempRT0"],"OutputLabels":["Screen","tempRT0"],"PassLabels":["0"],"VariableLabels":["None"],"Input":0,"Output":0,"Pass":0,"Iterations":1,"Variable":0}],"passOptions":["0"],"inputOptions":["Game","tempRT0"],"outputOptions":["Screen","tempRT0"],"variableOptions":["None"]}
//\	CameraOutput\	2880.467\	-334.7601\	192\	215\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			17\			0\	CameraInput\	655.7069\	-184.0458\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			3\			7\		False\			null\			null\		False\			null\			null\		True\			2\			2\	StereoUV\	-183.3421\	-161.9034\	192\	175\		False\			null\			null\		True\			12\			1\		True\			1\			6\	Variable3\	1031.588\	-298.9695\	192\	415\		/0\		/0\		True\			4\			1\		True\			4\			2\		True\			4\			3\		False\			null\			null\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			1\			3\	Add\	1436.03\	-187.8584\	192\	255\		/5\		True\			5\			1\		True\			3\			0\		True\			3\			1\		True\			3\			2\		False\			null\			null\	Div\	1740.475\	-45.63615\	192\	215\		/4\		True\			17\			1\		True\			4\			0\		True\			6\			0\		False\			null\			null\	Value1\	1409.364\	158.8082\	192\	95\		/3.0\		True\			5\			2\	Mul\	2326.706\	108.2935\	192\	215\		/4\		True\			17\			2\		True\			5\			0\		True\			19\			0\		False\			null\			null\	Abs\	1559.683\	316.2696\	192\	135\		True\			15\			1\		True\			9\			0\	Sin\	1251.586\	346.2696\	192\	135\		True\			8\			1\		True\			10\			0\	Mul\	808.2534\	289.6028\	192\	215\		/4\		True\			9\			1\		True\			12\			0\		True\			11\			0\		False\			null\			null\	Value1\	413.4919\	363.4126\	192\	95\		/16.0\		True\			10\			2\	Add\	130.8729\	105.554\	192\	215\		/4\		True\			10\			1\		True\			2\			1\		True\			13\			0\		False\			null\			null\	Time\	-488.9292\	210.834\	192\	255\		True\			12\			2\		False\			null\			null\		False\			null\			null\		False\			null\			null\		False\			null\			null\	Floor\	1951.786\	356.9054\	192\	135\		True\			19\			1\		True\			15\			0\	Mul\	1665.396\	533.9479\	192\	215\		/4\		True\			19\			2\		True\			8\			0\		True\			16\			0\		False\			null\			null\	Value1\	1302.5\	650.8347\	192\	95\		/3.0\		True\			15\			2\	Lerp\	2268.214\	-352.0231\	192\	215\		True\			0\			3\		True\			5\			0\		True\			7\			0\		True\			18\			0\	_FloatSlider\	1913.928\	-166.309\	192\	95\		/Lines\		/1\		/-1\		True\			17\			3\	Lerp\	2385\	589.9991\	192\	215\		True\			7\			2\		True\			14\			0\		True\			15\			0\		True\			20\			0\	Value1\	1980\	719.9991\	192\	95\		/0.5\		True\			19\			3

Shader "NTEC/IndustrialCamera" {

	SubShader {
		Cull Off ZWrite Off ZTest Always

		Pass {
			HLSLPROGRAM
			#pragma vertex VertDefault
			#pragma fragment Frag

			#include "PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);

			uniform half _Lines;

			half4 Frag (VaryingsDefault i) : SV_Target {
				half3 var0 = 0.0;
				half4 CameraOutput = 0.0;
				var0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo).rgb;
				CameraOutput.rgb = lerp(((var0.x + var0.y + var0.z) / 3.0),(((var0.x + var0.y + var0.z) / 3.0) * lerp(floor((abs(sin(((i.texcoordStereo.y + _Time.x) * 16.0))) * 3.0)),(abs(sin(((i.texcoordStereo.y + _Time.x) * 16.0))) * 3.0),0.5)),_Lines);
				return CameraOutput;
			}
			ENDHLSL
		}
	}
}