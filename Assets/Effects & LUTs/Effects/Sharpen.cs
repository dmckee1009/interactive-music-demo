using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(SharpenRenderer), PostProcessEvent.BeforeStack, "NTEC/Sharpen")]
	public sealed class Sharpen : PostProcessEffectSettings
	{
		[Range(0f, 1f), Tooltip("Intensity of sharpening")]
		public FloatParameter Intensity = new FloatParameter {value = 0f};
		internal RenderTexture tempRT0;
	}

	public sealed class SharpenRenderer : PostProcessEffectRenderer<Sharpen>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/Sharpen"));
			sheet.properties.SetFloat("_Intensity", settings.Intensity);
			settings.tempRT0 = RenderTexture.GetTemporary(Screen.width, Screen.height);
			context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
			RenderTexture.ReleaseTemporary(settings.tempRT0);
		}
	}
}