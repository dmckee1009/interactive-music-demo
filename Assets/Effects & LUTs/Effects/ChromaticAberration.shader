//{"Values":["0","NTEC/ChromaticAberration","_MainTex","0",""]}|{"position":{"serializedVersion":"2","x":0.0,"y":0.0,"width":212.0,"height":109.0},"name":"FloatSlider","selected":false,"Values":["Offset","Channels offset","0","0","0.01"],"serial":0,"unique":-1,"type":"FloatSliderField"}|{"tempTextures":0,"passes":[{"position":{"serializedVersion":"2","x":0.0,"y":36.0,"width":212.0,"height":16.0},"InputLabels":["Game"],"OutputLabels":["Screen"],"PassLabels":["0"],"VariableLabels":["None"],"Input":0,"Output":0,"Pass":0,"Iterations":1,"Variable":0}],"passOptions":["0"],"inputOptions":["Game"],"outputOptions":["Screen"],"variableOptions":["None"]}
//\	CameraOutput\	2539.737\	44.9517\	192\	215\		True\			1\			0\		True\			3\			1\		True\			4\			2\		False\			null\			null\	CameraInput\	1719.737\	-97.54837\	192\	335\		True\			0\			0\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			7\			0\		True\			2\			1\		False\			null\			null\	StereoUV\	882.2369\	319.9517\	192\	175\		True\			7\			1\		True\			4\			5\		True\			3\			6\	CameraInput\	1719.737\	263.0516\	192\	335\		False\			null\			null\		True\			0\			1\		False\			null\			null\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			2\			2\	CameraInput\	1719.737\	623.6516\	192\	335\		False\			null\			null\		False\			null\			null\		True\			0\			2\		False\			null\			null\		True\			6\			0\		True\			2\			1\		False\			null\			null\	_FloatSlider\	996.4288\	862.501\	192\	95\		/Offset\		/1\		/-1\		True\			7\			2\	Add\	1360\	687.4991\	192\	215\		/4\		True\			4\			4\		True\			2\			0\		True\			5\			0\		False\			null\			null\	Sub\	1317.5\	64.99903\	192\	215\		/4\		True\			1\			4\		True\			2\			0\		True\			5\			0\		False\			null\			null

Shader "NTEC/ChromaticAberration" {

	SubShader {
		Cull Off ZWrite Off ZTest Always

		Pass {
			HLSLPROGRAM
			#pragma vertex VertDefault
			#pragma fragment Frag

			#include "PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);

			uniform half _Offset;

			half4 Frag (VaryingsDefault i) : SV_Target {
				half4 CameraOutput = 0.0;
				CameraOutput.r = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2((i.texcoordStereo.x - _Offset),i.texcoordStereo.y)).r;
				CameraOutput.g = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo).g;
				CameraOutput.b = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2((i.texcoordStereo.x + _Offset),i.texcoordStereo.y)).b;
				return CameraOutput;
			}
			ENDHLSL
		}
	}
}