//{"Values":["0","NTEC/Blur","_MainTex","0",""]}|{"position":{"serializedVersion":"2","x":0.0,"y":0.0,"width":212.0,"height":109.0},"name":"FloatSlider","selected":false,"Values":["Intensity","Effect Intensity","0","0","0.005"],"serial":0,"unique":-1,"type":"FloatSliderField"}|{"position":{"serializedVersion":"2","x":0.0,"y":126.0,"width":212.0,"height":109.0},"name":"IntSlider","selected":false,"Values":["Iterations","Effect Iterations","1","1","32"],"serial":1,"unique":3980,"type":"IntSliderField"}|{"tempTextures":0,"passes":[{"position":{"serializedVersion":"2","x":0.0,"y":36.0,"width":212.0,"height":16.0},"InputLabels":["Game"],"OutputLabels":["Screen"],"PassLabels":["0"],"VariableLabels":["None","Iterations"],"Input":0,"Output":0,"Pass":0,"Iterations":0,"Variable":1}],"passOptions":["0"],"inputOptions":["Game"],"outputOptions":["Screen"],"variableOptions":["None","Iterations"]}
//\	CameraOutput\	4431.086\	273.5829\	192\	215\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			8\			0\	CameraInput\	2237.588\	-23.08383\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			4\			1\		True\			5\			0\		True\			2\			1\		False\			null\			null\	StereoUV\	1272.191\	332.4716\	192\	175\		True\			6\			1\		True\			3\			5\		False\			null\			null\	CameraInput\	2254.255\	374.049\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			4\			2\		True\			6\			0\		True\			2\			1\		False\			null\			null\	Add\	3393.146\	955.582\	192\	295\		/6\		True\			8\			1\		True\			1\			3\		True\			3\			3\		True\			10\			3\		True\			12\			3\		False\			null\			null\	Add\	1766.477\	188.9604\	192\	215\		/4\		True\			1\			4\		True\			2\			0\		True\			7\			0\		False\			null\			null\	Sub\	1827.112\	654.6283\	192\	215\		/4\		True\			3\			4\		True\			2\			0\		True\			7\			0\		False\			null\			null\	_FloatSlider\	1311.938\	704.8821\	192\	95\		/Intensity\		/1\		/-1\		True\			6\			2\	Div\	3850.539\	504.661\	192\	215\		/4\		True\			0\			3\		True\			4\			0\		True\			9\			0\		False\			null\			null\	Value1\	3713.397\	1003.588\	192\	95\		/4.0\		True\			8\			2\	CameraInput\	2237.588\	907.922\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			4\			3\		True\			11\			0\		True\			13\			0\		False\			null\			null\	StereoUV\	1272.191\	1263.478\	192\	175\		True\			12\			4\		True\			14\			1\		False\			null\			null\	CameraInput\	2254.255\	1305.056\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			4\			4\		True\			11\			0\		True\			14\			0\		False\			null\			null\	Add\	1766.477\	1119.966\	192\	215\		/4\		True\			10\			5\		True\			11\			1\		True\			15\			0\		False\			null\			null\	Sub\	1827.112\	1585.637\	192\	215\		/4\		True\			12\			5\		True\			11\			1\		True\			15\			0\		False\			null\			null\	_FloatSlider\	1303.938\	1540.557\	192\	95\		/Intensity\		/1\		/-1\		True\			14\			2

Shader "NTEC/Blur" {

	SubShader {
		Cull Off ZWrite Off ZTest Always

		Pass {
			HLSLPROGRAM
			#pragma vertex VertDefault
			#pragma fragment Frag

			#include "PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);

			uniform half _Intensity;
			uniform half _Iterations;

			half4 Frag (VaryingsDefault i) : SV_Target {
				half4 CameraOutput = 0.0;
				CameraOutput.rgb = ((SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2((i.texcoordStereo.x + _Intensity),i.texcoordStereo.y)).rgb + SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2((i.texcoordStereo.x - _Intensity),i.texcoordStereo.y)).rgb + SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2(i.texcoordStereo.x,(i.texcoordStereo.y + _Intensity))).rgb + SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, half2(i.texcoordStereo.x,(i.texcoordStereo.y - _Intensity))).rgb) / 4.0);
				return CameraOutput;
			}
			ENDHLSL
		}
	}
}