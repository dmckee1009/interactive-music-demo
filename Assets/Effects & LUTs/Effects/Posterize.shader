//{"Values":["0","NTEC/Posterize","_MainTex","0",""]}|{"position":{"serializedVersion":"2","x":0.0,"y":0.0,"width":212.0,"height":89.0},"name":"Int","selected":false,"Values":["Colors","Number of colors in each channel","0"],"serial":0,"unique":-1,"type":"IntField"}|{"tempTextures":0,"passes":[{"position":{"serializedVersion":"2","x":0.0,"y":36.0,"width":212.0,"height":16.0},"InputLabels":["Game"],"OutputLabels":["Screen"],"PassLabels":["0"],"VariableLabels":["None","Colors"],"Input":0,"Output":0,"Pass":0,"Iterations":1,"Variable":0}],"passOptions":["0"],"inputOptions":["Game"],"outputOptions":["Screen"],"variableOptions":["None","Colors"]}
//\	CameraOutput\	2660.448\	149.0557\	192\	215\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			7\			0\	StereoUV\	711.5687\	308.0796\	192\	175\		False\			null\			null\		False\			null\			null\		True\			5\			6\	Div\	2163.283\	508.9366\	192\	215\		/4\		True\			7\			2\		True\			4\			0\		True\			6\			0\		False\			null\			null\	Mul\	1443.282\	460.9366\	192\	215\		/4\		True\			4\			1\		True\			5\			3\		True\			6\			0\		False\			null\			null\	Floor\	1789.282\	496.9366\	192\	135\		True\			2\			1\		True\			3\			0\	CameraInput\	1118.901\	143.2699\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			7\			3\		False\			null\			null\		False\			null\			null\		True\			1\			2\	_Int\	995.584\	759.1605\	192\	95\		/Colors\		/1\		/-1\		True\			7\			1\	If\	2242.5\	102.499\	192\	215\		True\			0\			3\		True\			6\			0\		True\			2\			0\		True\			5\			3

Shader "NTEC/Posterize" {

	SubShader {
		Cull Off ZWrite Off ZTest Always

		Pass {
			HLSLPROGRAM
			#pragma vertex VertDefault
			#pragma fragment Frag

			#include "PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);

			uniform half _Colors;

			half4 Frag (VaryingsDefault i) : SV_Target {
				half4 CameraOutput = 0.0;
				CameraOutput.rgb = (_Colors ? (floor((SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo).rgb * _Colors)) / _Colors) : SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo).rgb);
				return CameraOutput;
			}
			ENDHLSL
		}
	}
}