using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(ChromaticAberrationRenderer), PostProcessEvent.AfterStack, "NTEC/ChromaticAberration")]
	public sealed class ChromaticAberration : PostProcessEffectSettings
	{
		[Range(0f, 0.01f), Tooltip("Channels offset")]
		public FloatParameter Offset = new FloatParameter {value = 0f};
	}

	public sealed class ChromaticAberrationRenderer : PostProcessEffectRenderer<ChromaticAberration>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/ChromaticAberration"));
			sheet.properties.SetFloat("_Offset", settings.Offset);
			context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
		}
	}
}