using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(IndustrialCameraRenderer), PostProcessEvent.AfterStack, "NTEC/IndustrialCamera")]
	public sealed class IndustrialCamera : PostProcessEffectSettings
	{
		[Range(0f, 1f), Tooltip("Lines visibility")]
		public FloatParameter Lines = new FloatParameter {value = 0f};
		internal RenderTexture tempRT0;
	}

	public sealed class IndustrialCameraRenderer : PostProcessEffectRenderer<IndustrialCamera>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/IndustrialCamera"));
			sheet.properties.SetFloat("_Lines", settings.Lines);
			settings.tempRT0 = RenderTexture.GetTemporary(Screen.width, Screen.height);
			context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
			RenderTexture.ReleaseTemporary(settings.tempRT0);
		}
	}
}