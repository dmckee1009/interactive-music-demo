using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(BlurRenderer), PostProcessEvent.AfterStack, "NTEC/Blur")]
	public sealed class Blur : PostProcessEffectSettings
	{
		[Range(0f, 0.005f), Tooltip("Effect Intensity")]
		public FloatParameter Intensity = new FloatParameter {value = 0f};
		[Range(1, 32), Tooltip("Effect Iterations")]
		public IntParameter Iterations = new IntParameter {value = 1};
		internal RenderTexture loopRT0;
		internal RenderTexture loopRT1;
	}

	public sealed class BlurRenderer : PostProcessEffectRenderer<Blur>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/Blur"));
			sheet.properties.SetFloat("_Intensity", settings.Intensity);
			sheet.properties.SetFloat("_Iterations", settings.Iterations);
			settings.loopRT0 = RenderTexture.GetTemporary(Screen.width, Screen.height);
			context.command.BlitFullscreenTriangle(context.source, settings.loopRT0);
			for (int i = 0; i < settings.Iterations; ++i)
			{
				settings.loopRT1 = RenderTexture.GetTemporary(Screen.width, Screen.height);
				context.command.BlitFullscreenTriangle(settings.loopRT0, settings.loopRT1, sheet, 0);
				RenderTexture.ReleaseTemporary(settings.loopRT0);
				settings.loopRT0 = settings.loopRT1;
			}
			context.command.BlitFullscreenTriangle(settings.loopRT0, context.destination);
			RenderTexture.ReleaseTemporary(settings.loopRT0);
		}
	}
}