//{"Values":["0","NTEC/Fog","_MainTex","0",""]}|{"position":{"serializedVersion":"2","x":0.0,"y":0.0,"width":212.0,"height":109.0},"name":"FloatSlider","selected":false,"Values":["Distance","Distance at which fog should appear","0","0","1"],"serial":0,"unique":-1,"type":"FloatSliderField"}|{"position":{"serializedVersion":"2","x":0.0,"y":126.0,"width":212.0,"height":109.0},"name":"FloatSlider","selected":false,"Values":["Intensity","Fog visibility","0","0","1"],"serial":1,"unique":2153,"type":"FloatSliderField"}|{"tempTextures":0,"passes":[{"position":{"serializedVersion":"2","x":0.0,"y":36.0,"width":212.0,"height":16.0},"InputLabels":["Game"],"OutputLabels":["Screen"],"PassLabels":["0"],"VariableLabels":["None"],"Input":0,"Output":0,"Pass":0,"Iterations":1,"Variable":0}],"passOptions":["0"],"inputOptions":["Game"],"outputOptions":["Screen"],"variableOptions":["None"]}
//\	CameraOutput\	2857.675\	261.4386\	192\	215\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			16\			0\	StereoUV\	-49.02551\	796.6752\	192\	175\		False\			null\			null\		False\			null\			null\		True\			3\			6\	CameraDepth\	386.8885\	1064.522\	192\	215\		True\			14\			2\		False\			null\			null\		False\			null\			null\		True\			1\			2\	CameraInput\	1303.258\	473.4866\	192\	335\		False\			null\			null\		False\			null\			null\		False\			null\			null\		True\			15\			1\		False\			null\			null\		False\			null\			null\		True\			1\			2\	Value1\	628.8301\	914.7732\	192\	95\		/1.0\		True\			14\			1\	Mul\	1985.236\	746.0819\	192\	215\		/4\		True\			13\			1\		True\			9\			0\		True\			6\			0\		False\			null\			null\	_FloatSlider\	2146.189\	1609.846\	192\	95\		/Intensity\		/2\		/2153\		True\			5\			2\	_FloatSlider\	423.6656\	1645.415\	192\	95\		/Distance\		/1\		/-1\		True\			10\			1\	Value1\	791.2915\	1804.933\	192\	95\		/1.0\		True\			10\			2\	Mul\	1691.85\	1385.485\	192\	215\		/4\		True\			5\			1\		True\			11\			0\		True\			10\			0\		False\			null\			null\	Mul\	1196.847\	1570.533\	192\	215\		/4\		True\			9\			2\		True\			7\			0\		True\			8\			0\		False\			null\			null\	Pow\	1291.887\	1302.549\	192\	175\		True\			9\			1\		True\			14\			0\		True\			12\			0\	Value1\	508.5538\	1310.882\	192\	95\		/3.0\		True\			11\			2\	Saturate\	2659.61\	1136.927\	192\	135\		True\			16\			3\		True\			5\			0\	Sub\	958.8321\	1070.532\	192\	215\		/4\		True\			11\			1\		True\			4\			0\		True\			2\			0\		False\			null\			null\	Saturate\	2050.064\	467.8049\	192\	135\		True\			16\			1\		True\			3\			3\	Lerp\	2562.341\	463.2152\	192\	215\		True\			0\			3\		True\			15\			0\		True\			17\			0\		True\			13\			0\	Value1\	2085.715\	257.144\	192\	95\		/0.75\		True\			16\			2

Shader "NTEC/Fog" {

	SubShader {
		Cull Off ZWrite Off ZTest Always

		Pass {
			HLSLPROGRAM
			#pragma vertex VertDefault
			#pragma fragment Frag

			#include "PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
			TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);

			uniform half _Distance;
			uniform half _Intensity;

			half4 Frag (VaryingsDefault i) : SV_Target {
				half4 CameraOutput = 0.0;
				CameraOutput.rgb = lerp(saturate(SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoordStereo).rgb),0.75,saturate(((pow((1.0 - SAMPLE_TEXTURE2D(_CameraDepthTexture, sampler_CameraDepthTexture, i.texcoordStereo).x),3.0) * (_Distance * 1.0)) * _Intensity)));
				return CameraOutput;
			}
			ENDHLSL
		}
	}
}