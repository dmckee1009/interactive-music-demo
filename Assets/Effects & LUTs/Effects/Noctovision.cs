using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(NoctovisionRenderer), PostProcessEvent.AfterStack, "NTEC/Noctovision")]
	public sealed class Noctovision : PostProcessEffectSettings
	{
		[Range(0f, 1f), Tooltip("Noise intenisty")]
		public FloatParameter Noise = new FloatParameter {value = 0f};
		internal RenderTexture tempRT0;
	}

	public sealed class NoctovisionRenderer : PostProcessEffectRenderer<Noctovision>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/Noctovision"));
			sheet.properties.SetFloat("_Noise", settings.Noise);
			settings.tempRT0 = RenderTexture.GetTemporary(Screen.width, Screen.height);
			context.command.BlitFullscreenTriangle(context.source, settings.tempRT0, sheet, 0);
			context.command.BlitFullscreenTriangle(settings.tempRT0, context.destination, sheet, 1);
			RenderTexture.ReleaseTemporary(settings.tempRT0);
		}
	}
}