using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace NTEC.PPU
{
	[Serializable]
	[PostProcess(typeof(FogRenderer), PostProcessEvent.AfterStack, "NTEC/Fog")]
	public sealed class Fog : PostProcessEffectSettings
	{
		[Range(0f, 1f), Tooltip("Distance at which fog should appear")]
		public FloatParameter Distance = new FloatParameter {value = 0f};
		[Range(0f, 1f), Tooltip("Fog visibility")]
		public FloatParameter Intensity = new FloatParameter {value = 0f};
	}

	public sealed class FogRenderer : PostProcessEffectRenderer<Fog>
	{
		public override void Render(PostProcessRenderContext context)
		{
			var sheet = context.propertySheets.Get(Shader.Find("NTEC/Fog"));
			sheet.properties.SetFloat("_Distance", settings.Distance);
			sheet.properties.SetFloat("_Intensity", settings.Intensity);
			context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
		}
	}
}