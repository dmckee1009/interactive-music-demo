﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyRotation : MonoBehaviour
{

    public float rotation = 0;
    public float blend = 0;
    public float rotationSpeed = 0;
    public float exposure = 1;

	// Use this for initialization
	void Start ()
    {
       // RenderSettings.skybox.SetFloat("_Blend", 1.0f);

    }

    // Update is called once per frame
    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", rotation);
        RenderSettings.skybox.SetFloat("_Exposure", exposure);
        RenderSettings.skybox.SetFloat("_SkyBlend", blend);
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * rotationSpeed);
    }

}
