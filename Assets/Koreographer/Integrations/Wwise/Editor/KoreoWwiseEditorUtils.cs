﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using SonicBloom.Koreo.Players.Wwise;

namespace SonicBloom.Koreo.EditorUI.WwiseTools
{
	/// <summary>
	/// This class updates Koreographer's settings for the Wwise integration workflow.
	/// </summary>
	[InitializeOnLoad]
	public class KoreoWwiseEditor
	{
		static KoreoWwiseEditor()
		{
			EnableWwiseIntegration();
		}

		static void EnableWwiseIntegration()
		{
			// Enable editing Koreography with Audio Files instead of Unity AudioClip assets.
			KoreographyEditor.ShowAudioFileImportOption = true;
		}

		[MenuItem("Assets/Create/Wwise Koreography Set")]
		public static void CreateWwiseKoreographySetAsset()
		{
			WwiseKoreographySet asset = ScriptableObject.CreateInstance<WwiseKoreographySet>();
			ProjectWindowUtil.CreateAsset(asset, "New " + typeof(WwiseKoreographySet).Name + ".asset");
		}
	}
}
