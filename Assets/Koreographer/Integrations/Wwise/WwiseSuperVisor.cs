//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;

namespace SonicBloom.Koreo.Players.Wwise
{
	/// <summary>
	/// The WwiseSuperVisor provides Koreographer event system integration support
	/// to a Wwise-based project. Koreography is loaded through a special
	/// <c>WwiseKoreographySet</c> asset that must be created in the Editor. This
	/// system integrates with Wwise through the Wwise callback system. Please
	/// see the HandleAKCallback method for more information. Callbacks from Wwise
	/// AkEvent components are also supported. To make use of them, specify the
	/// GameObject to which the WwiseSuperVisor is connected, write
	/// "HandleAKEventCallback" in the "Callback Function" field, and be sure to
	/// set the "EndOfEvent", "Duration", and "EnableGetSourcePlayPosition"
	/// Callback Flags.
	/// </summary>
	[AddComponentMenu("Koreographer/Wwise/Wwise Super Visor")]
	public class WwiseSuperVisor : MonoBehaviour
	{
		#region Fields
		
		[SerializeField]
		[Tooltip("Koreography for any sound expected to be played in this level/scene.")]
		WwiseKoreographySet koreographySet;
		
		[Header("Optional")]
		
		[SerializeField]
		[Tooltip("A specific Koreographer component to use for event triggering.")]
		Koreographer targetKoreographer = null;
		
		// Track allocated visors to cut down on in-game allocations (pooling).
		Stack<WwisePlayingIDVisor> availableVisors = new Stack<WwisePlayingIDVisor>();
		// Active visors are Updated in Update().
		List<WwisePlayingIDVisor> activeVisors = new List<WwisePlayingIDVisor>();
		
		#endregion
		#region Methods
		
		WwisePlayingIDVisor GetAvailableVisor()
		{
			WwisePlayingIDVisor visor = null;
			
			if (availableVisors.Count > 0)
			{
				visor = availableVisors.Pop();
			}
			else
			{
				visor = new WwisePlayingIDVisor();
			}
			
			return visor;
		}

		void DeactivateVisor(uint playingID)
		{
			for (int i = 0; i < activeVisors.Count; ++i)
			{
				WwisePlayingIDVisor visor = activeVisors[i];

				if (visor.PlayingID == playingID)
				{
					activeVisors.RemoveAt(i);
					visor.Reset();
					availableVisors.Push(visor);
					break;
				}
			}
		}

		void LoadKoreography()
		{
			if (targetKoreographer != null && koreographySet != null)
			{
				List<WwiseKoreoMediaIDEntry> koreographies = koreographySet.koreographies;
				
				for (int i = 0; i < koreographies.Count; ++i)
				{
					targetKoreographer.LoadKoreography(koreographies[i].koreo);
				}
			}
		}

		void UnloadKoreography()
		{
			if (targetKoreographer != null && koreographySet != null)
			{
				List<WwiseKoreoMediaIDEntry> koreographies = koreographySet.koreographies;
				
				for (int i = 0; i < koreographies.Count; ++i)
				{
					targetKoreographer.UnloadKoreography(koreographies[i].koreo);
				}
			}
		}
		
		/// <summary>
		/// Instructs the <c>WwiseSuperVisor</c> to begin watching the audio file played back
		/// with the specified <paramref name="mediaID"/> and <paramref name="audioNodeID"/> within
		/// the event specified by the <paramref name="playingID"/>. The parameters are typically
		/// returned by callbacks from the Wwise sound engine (AK_Duration).
		/// </summary>
		/// <param name="playingID">The Wwise playingID for the audio file to watch.</param>
		/// <param name="mediaID">The mediaID of the audio file to watch.</param>
		/// <param name="audioNodeID">The audioNodeID of the instance of the audio file to watch.</param>
		/// <param name="duration">The duration of the audio file associated with the <paramref name="playbackID"/>.</param>
		void WatchAudioPlayback(uint playingID, uint mediaID, uint audioNodeID, float duration)
		{
			if (koreographySet != null)
			{
				// Locate koreography associated with the mediaID.
				Koreography koreo = null;

				// Search with a for-loop rather than List.Find to avoid allocations.
				List<WwiseKoreoMediaIDEntry> koreos = koreographySet.koreographies;
				for (int i = 0; i < koreos.Count; ++i)
				{
					if (koreos[i].MediaID == mediaID)
					{
						koreo = koreos[i].koreo;
						break;
					}
				}

				// Verify that we have Koreography to watch.
				if (koreo != null)
				{
					// Locate a pre-existing visor associated with this playingID (if one exists).
					WwisePlayingIDVisor visor = null;
					
					// Search with a for-loop rather than List.Find to avoid allocations.
					for (int i = 0; i < activeVisors.Count; ++i)
					{
						if (activeVisors[i].PlayingID == playingID)
						{
							visor = activeVisors[i];
							break;
						}
					}
					
					// If an associated PlayingID visor doesn't exist, initialize one.
					if (visor == null)
					{
						visor = GetAvailableVisor();
						visor.Init(playingID, targetKoreographer);
						activeVisors.Add(visor);
					}
					
					// Start watching the playback of the specified MediaID + AudioNodeID combo.
					visor.BeginWatchingPlayback(mediaID, audioNodeID, koreo.SourceClipName, koreo.SampleRate, duration);
				}
			}
		}
		
		void Start()
		{
			if (targetKoreographer == null)
			{
				targetKoreographer = Koreographer.Instance;
			}
			
			if (targetKoreographer == null)
			{
				Debug.LogWarning("No Koreographer component specified and could not find the singleton.  Please add a Koreographer " +
				                 "component to the scene or make sure to specify a default Koreographer. Disabling this WwiseSuperVisor.");
				enabled = false;
			}
			else
			{
				LoadKoreography();
			}
		}
		
		void Update()
		{
			for (int i = 0; i < activeVisors.Count; ++i)
			{
				activeVisors[i].Update();
			}
		}

		void OnDestroy()
		{
			UnloadKoreography();
		}
		
		#endregion
		#region Wwise Interface Methods
		
		/// <summary>
		/// <para>The callback that collects necessary info and hands it to a <c>WwisePlayingIDVisor</c> for
		/// sync. If you handle <c>PostEvent</c> callbacks yourself to retrieve your own info, please also
		/// forward the callbacks to this callback handler.</para>
		/// <para>To use this successfully, please specify the following callback flags in the call to
		/// <c>PostEvent</c> or equivalent:</para>
		/// <para> - AK_Duration</para>
		/// <para> - AK_EnableGetSourcePlayPosition</para>
		/// <para> - AK_EndOfEvent</para>
		/// </summary>
		/// <param name="in_cookie">An <c>object</c> passed in when the Wwise Event was posted. Not used.</param>
		/// <param name="in_type">The type of callback this is.</param>
		/// <param name="in_info">The callback payload.</param>
		public void HandleAKCallback(object in_cookie, AkCallbackType in_type, object in_info)
		{
			if (in_type == AkCallbackType.AK_Duration)
			{
				// Get the duration and Media ID and begin watching playback.
				AkDurationCallbackInfo theInfo = (AkDurationCallbackInfo)in_info;
				
				WatchAudioPlayback(theInfo.playingID, theInfo.mediaID, theInfo.audioNodeID, theInfo.fDuration);
			}
			else if (in_type == AkCallbackType.AK_EndOfEvent)
			{
				// Reset a PlayingID Visor as the event has ended.
				AkEventCallbackInfo theInfo = (AkEventCallbackInfo)in_info;

				DeactivateVisor(theInfo.playingID);
			}
		}
		
		// This is a callback handler designed to be connected with the AkEvent component. This allows you to set up
		//  event playback without having to write any script.
		//  Currently you must configure the AkEvent to include the following Callback Flags:
		//   - EndOfEvent
		//   - Duration
		//   - EnableGetSourcePlayPosition
		void HandleAKEventCallback(object in_callbackInfo)
		{
			// Note: AkEventCallbackMsg is a struct. Wrapping this type in the "object" type probably introduces some
			//  boxing allocations.
			AkEventCallbackMsg callbackInfo = (AkEventCallbackMsg)in_callbackInfo;

			// Hand the unpacked info off to the main handler.
			HandleAKCallback(null, callbackInfo.type, callbackInfo.info);
		}
		
		#endregion
	}
}
