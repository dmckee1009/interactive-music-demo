﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;

namespace SonicBloom.Koreo.Players.Wwise
{
	/// <summary>
	/// The WwiseMediaIDVisor is built on top of VisorBase, specifically adding
	/// the core features required to support the Wwise audio system integration.
	/// It currently handles basic update functionality but must be initialized by
	/// an external system.
	/// </summary>
	public class WwiseMediaIDVisor : VisorBase
	{
		#region Data Fields

		/// <summary>
		/// The MediaID is what maps this to an actual audio file as listed in
		/// the SoundbanksInfo.xml file.
		/// </summary>
		uint mediaID = 0u;

		/// <summary>
		/// The AudioNodeID is used by Wwise to specify a location of the
		/// file's use in the internal hierarchy of the Wwise project. It can
		/// help differentiate between two separate playing instances of the
		/// same file.
		/// </summary>
		uint audioNodeID = 0u;

		/// <summary>
		/// The name of the audio file to which the MediaID relates.
		/// </summary>
		string audioName = string.Empty;

		/// <summary>
		/// The sample rate at which the Koreography was authored in terms of
		/// samples-per-millisecond
		/// </summary>
		float samplesPerMS = 0f;

		/// <summary>
		/// The total number of samples to be played back.
		/// </summary>
		int sampleDuration = 0;

		#endregion
		#region Runtime Fields

		/// <summary>
		/// The most recent audio position received from Wwise in samples.
		/// </summary>
		int wwiseSampleTime = -1;

		/// <summary>
		/// The change in sample position between the current and previous
		/// update passes.
		/// </summary>
		int wwiseDeltaSamples = 0;

		/// <summary>
		/// MediaIDs and duration typically come via the Duration Callback.
		/// There appears to occasionally be a gap frame (or frames) where audio
		/// does not actually play even though we were given the heads up.  This
		/// allows us to only remove the mapping from consideration once it's
		/// actually received at least a single update (i.e. it's position was
		/// reported at least once).
		/// </summary>
		bool bHasBeenUpdated = false;

		#endregion
		#region Properties

		/// <summary>
		/// Gets the MediaID that this <c>WwiseMediaIDVisor</c> is watching.
		/// </summary>
		/// <value>The MediaID.</value>
		public uint MediaID
		{
			get
			{
				return mediaID;
			}
		}

		/// <summary>
		/// Gets the AudioNodeID that this <c>WwiseMediaIDVisor</c> is watching.
		/// </summary>
		/// <value>The AudioNodeID.</value>
		public uint AudioNodeID
		{
			get
			{
				return audioNodeID;
			}
		}

		/// <summary>
		/// Whether or not position reporting has started or not.
		/// </summary>
		/// <value><c>true</c> if position reporting has started; otherwise, <c>false</c>.</value>
		public bool HasStarted
		{
			get
			{
				return bHasBeenUpdated;
			}
		}

		/// <summary>
		/// Whether or not the audio is playing (specifically: not paused or stopped).
		/// </summary>
		/// <value><c>true</c> if the audio is playing; otherwise, <c>false</c>.</value>
		public bool IsPlaying
		{
			get
			{
				return GetIsAudioPlaying();
			}
		}

		/// <summary>
		/// Retrieves the <c>Koreographer</c> component used by this
		/// <c>WwiseMediaIDVisor</c>.
		/// </summary>
		/// <value>The <c>Koreographer</c> component.</value>
		public Koreographer KoreographerCom
		{
			get
			{
				return koreographerCom;
			}
		}

		/// <summary>
		/// Gets the name of the audio file being tracked.
		/// </summary>
		/// <value>The name of the audio file being tracked.</value>
		public string AudioName
		{
			get
			{
				return audioName;
			}
		}

		/// <summary>
		/// Gets the latest sample time equivalent of the audio position reported by Wwise.
		/// </summary>
		/// <value>The sample time of the audio.</value>
		public int SampleTime
		{
			get
			{
				return wwiseSampleTime;
			}
		}

		/// <summary>
		/// Gets the total number of samples to be played back.
		/// </summary>
		/// <value>The total number of samples to be played back.</value>
		public int SampleDuration
		{
			get
			{
				return sampleDuration;
			}
		}

		#endregion
		#region Initializers

		/// <summary>
		/// Initializes the <c>WwiseMediaIDVisor</c>.
		/// </summary>
		/// <param name="_mediaID">The Wwise-internal MediaID for the audio file.</param>
		/// <param name="_audioNodeID">The Wwise-internal AudioNodeID for this instance of the audio file.</param>
		/// <param name="_audioName">The name of the audio file being played back.</param>
		/// <param name="_sampleRate">The sample rate of the audio file as it was during Koreography authoring time.</param>
		/// <param name="_duration">The duration of the audio file in milliseconds (Wwise uses <c>float</c> for this).</param>
		/// <param name="targetKoreographer">The Koreographer to use.  If not specified, uses the default singleton.</param>
		public void Init(uint _mediaID, uint _audioNodeID, string _audioName, int _sampleRate, float _duration, Koreographer targetKoreographer = null)
		{
			// Setting up data.
			mediaID = _mediaID;
			audioNodeID = _audioNodeID;
			audioName = _audioName;
			samplesPerMS = _sampleRate / 1000f;
			sampleDuration = (int)((float)samplesPerMS * _duration);

			// Base class.
			koreographerCom = targetKoreographer != null ? targetKoreographer : Koreographer.Instance;
		}

		/// <summary>
		/// Resets this <c>WwiseMediaIDVisor</c> instance for reuse.
		/// </summary>
		public void Reset()
		{
			// Reset data fields.
			mediaID = 0u;
			audioNodeID = 0u;
			audioName = string.Empty;
			samplesPerMS = 0f;
			sampleDuration = 0;

			// Reset runtime fields.
			wwiseSampleTime = -1;
			wwiseDeltaSamples = 0;
			bHasBeenUpdated = false;

			// Base class.
			koreographerCom = null;
			sampleTime = 0;
			sourceSampleTime = 0;
		}

		#endregion
		#region Methods

		/// <summary>
		/// Updates the audio position.  Takes time in milliseconds as Wwise uses milliseconds
		/// for all timing.
		/// </summary>
		/// <param name="timeInMS">Time in milliseconds.  Converted to samples internally.</param>
		public void UpdateAudioPosition(int timeInMS)
		{
			// Convert milliseconds to samples.
			int newSampleTime = (int)((float)timeInMS * samplesPerMS);
			
			if (newSampleTime < wwiseSampleTime)
			{
				// TODO: Figure out what would cause this. Looped?  Stopped?  Played in reverse?
				//  Currently we just ignore this situation on purpose so as not to cause odd
				//  bugs. May not happen.
				wwiseSampleTime = newSampleTime;
			}

			// Store the delta and then update the core time.
			wwiseDeltaSamples = newSampleTime - wwiseSampleTime;
			wwiseSampleTime = newSampleTime;

			// Store the fact that we've been updated at least once!
			bHasBeenUpdated = true;
		}

		#endregion
		#region State Accessors

		/// <summary>
		/// Gets the name of the audio currently playing back.  This is used to identify Koreography for
		/// event triggering.
		/// </summary>
		/// <returns>The name of the currently playing audio.</returns>
		protected override string GetAudioName()
		{
			return audioName;
		}
		
		/// <summary>
		/// Gets whether or not the audio is currently playing back.
		/// </summary>
		/// <returns><c>true</c> if the audio is playing, <c>false</c> otherwise.</returns>
		protected override bool GetIsAudioPlaying()
		{
			return bHasBeenUpdated && (wwiseSampleTime != sourceSampleTime);
		}
		
		/// <summary>
		/// Gets whether the audio is set to loop or not.
		/// </summary>
		/// <returns><c>true</c> if audio should be looping, <c>false</c> otherwise.</returns>
		protected override bool GetIsAudioLooping()
		{
			return false;
		}
		
		/// <summary>
		/// Gets whether the audio looped this frame or not.
		/// </summary>
		/// <returns><c>true</c> if the audio looped, <c>false</c> otherwise.</returns>
		protected override bool GetDidAudioLoop()
		{
			return false;
		}
		
		/// <summary>
		/// Get the current time in samples of the current audio (the playhead position in samples).
		/// </summary>
		/// <returns>The current sample time.</returns>
		protected override int GetAudioSampleTime()
		{
			return wwiseSampleTime;
		}
		
		/// <summary>
		/// Gets the first *playable* sample position of the current audio.
		/// </summary>
		/// <returns>The first playable sample position.</returns>
		protected override int GetAudioStartSampleExtent()
		{
			return 0;
		}
		
		/// <summary>
		/// Get the last *playable* sample position of the current audio.
		/// Assumes playback always starts at 0!
		/// </summary>
		/// <returns>The last playable sample position.</returns>
		protected override int GetAudioEndSampleExtent()
		{
			return sampleDuration;
		}
		
		/// <summary>
		/// Get the number of samples that were played in the last frame.  Be sure to consider current settings 
		/// and playback state.
		/// </summary>
		/// <returns>The delta time in samples.</returns>
		protected override int GetDeltaTimeInSamples()
		{
			return wwiseDeltaSamples;
		}

		#endregion
	}
}
