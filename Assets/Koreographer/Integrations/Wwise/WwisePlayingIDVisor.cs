﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;

namespace SonicBloom.Koreo.Players.Wwise
{
	/// <summary>
	/// The WwisePlayingIDVisor manages synchronization for audio files played
	/// within the scope of a single AkEvent. When an AkEvent resolves to a "play"
	/// event, the returned playingID will continue to encapsulate all audio
	/// played through that event until it is stopped. This includes for the
	/// dialogue system (which requires that the dynamic sequence be closed).
	/// Individual audio files are resolved through a combination of MediaID and
	/// AudioNodeID. The WwisePlayingIDVisor does not manage these itself, but
	/// manages the object that does: WwiseMediaIDVisors. The WwisePlayingIDVisor
	/// is the component that actually makes the request for playback position
	/// and forwards timing information to the WwiseMediaIDVisors for
	/// consumption. The WwisePlayingIDVisor also implements the
	/// <c>IKoreographedPlayer</c> interface methods, which means that one may
	/// be used as a Koreographer component's <c>musicPlayerController</c>.
	/// </summary>
	public class WwisePlayingIDVisor : IKoreographedPlayer
	{
		#region Fields
		
		// The PlayingID of the current AkEvent.
		uint playingID = 0u;
		
		// The Koreographer component to use for event triggering.
		Koreographer koreographerCom = null;
		
		// Track allocated layers to cut down on in-game allocations (pooling).
		Stack<WwiseMediaIDVisor> availableVisors = new Stack<WwiseMediaIDVisor>();
		// Active visors are updated in Update().
		List<WwiseMediaIDVisor> activeVisors = new List<WwiseMediaIDVisor>();
		// This list is used in Update() to help determine when a layer ends.  It helps
		//  reduce memory allocations.
		List<WwiseMediaIDVisor> visorsToProcess = new List<WwiseMediaIDVisor>();
		
		// Arrays used to retrieve playback info.
		const int DEFAULT_MAX_LAYERS = 32;
		uint[] audioNodeIDs = new uint[DEFAULT_MAX_LAYERS];
		uint[] mediaIDs = new uint[DEFAULT_MAX_LAYERS];
		int[] audioTimes = new int[DEFAULT_MAX_LAYERS];
		
		#endregion
		#region Properties

		/// <summary>
		/// Gets the PlayingID of audio being managed.
		/// </summary>
		/// <value>The PlayingID.</value>
		public uint PlayingID
		{
			get
			{
				return playingID;
			}
		}

		#endregion
		#region Methods
		
		// Grab a MediaID Visor from the pool.
		WwiseMediaIDVisor GetAvailableMediaIDVisor()
		{
			WwiseMediaIDVisor visor = null;

			if (availableVisors.Count > 0)
			{
				visor = availableVisors.Pop();
			}
			else
			{
				visor = new WwiseMediaIDVisor();
			}
			
			return visor;
		}
		
		// Return a MediaID Visor to the 'available' pool.
		void DeactivateMediaIDVisor(WwiseMediaIDVisor visor)
		{
			visor.Reset();
			
			activeVisors.Remove(visor);
			availableVisors.Push(visor);
		}

		// Return all 'active' MediaID Visors to the 'available' pool.
		void DeactivateActiveMediaIDVisors()
		{
			// Reset and return all WwiseMediaIDVisors.
			for (int i = 0; i < activeVisors.Count; ++i)
			{
				WwiseMediaIDVisor visor = activeVisors[i];
				
				visor.Reset();
				availableVisors.Push(visor);
			}
			
			// Clear out the active MediaID visors.
			activeVisors.Clear();
		}

		/// <summary>
		/// Initialize the WwisePlayingIDVisor with the specified <paramref name="_playingID"/>
		/// and <paramref name="_targetKoreographer"/>.
		/// </summary>
		/// <param name="_playingID">The PlayingID whose audio to manage.</param>
		/// <param name="_targetKoreographer">The Koreographer component that audio layers should use.</param>
		public void Init(uint _playingID, Koreographer _targetKoreographer = null)
		{
			playingID = _playingID;
			koreographerCom = _targetKoreographer;
		}

		/// <summary>
		/// Resets the WwisePlayingIDVisor to its initial state (or near enough).
		/// </summary>
		public void Reset()
		{
			playingID = 0u;
			koreographerCom = null;

			// Just in case things didn't go smoothly in the Update() call.
			visorsToProcess.Clear();

			// Ensure no MediaIDVisors remain active.
			DeactivateActiveMediaIDVisors();
		}

		/// <summary>
		/// Instructs the WwisePlayingIDVisor to start watching the audio resolved by the
		/// <paramref name="mediaID"/> and <paramref name="audioNodeID"/>. This will grab
		/// (or create) an available WwiseMediaIDVisor and initialize it for use.
		/// </summary>
		/// <param name="mediaID">The MediaID of the audio to watch.</param>
		/// <param name="audioNodeID">The AudioNodeID of the audio to watch.</param>
		/// <param name="audioName">The name of the audio clip described by the <paramref name="mediaID"/>.</param>
		/// <param name="sampleRate">The sample rate of the audio clip described by the <paramref name="mediaID"/>.</param>
		/// <param name="duration">The duration of the audio clip described by the <paramref name="mediaID"/>.</param>
		public void BeginWatchingPlayback(uint mediaID, uint audioNodeID, string audioName, int sampleRate, float duration)
		{	
			WwiseMediaIDVisor visor = GetAvailableMediaIDVisor();

			visor.Init(mediaID, audioNodeID, audioName, sampleRate, duration, koreographerCom);
			
			activeVisors.Add(visor);
		}

		/// <summary>
		/// Checks the play positions of audio files "within" the playingID and triggers
		/// timing updates where necessary.
		/// </summary>
		public void Update()
		{
			uint numLayers = (uint)mediaIDs.Length;
			AKRESULT result = AkSoundEngine.GetSourceMultiplePlayPositions(playingID, audioNodeIDs, mediaIDs, audioTimes, ref numLayers, true);
			
			if (result == AKRESULT.AK_Fail)
			{
				// Clear out any previously started layers.
				for (int i = activeVisors.Count - 1; i >= 0; --i)
				{
					WwiseMediaIDVisor visor = activeVisors[i];
					if (visor.HasStarted)
					{
						// Remove visors that have had a chance to start; playback failure
						//  indicates that playback is done for these.
						DeactivateMediaIDVisor(visor);
					}
				}
			}
			else if (result == AKRESULT.AK_Success)
			{
				// In case there was an uncaught exception (or the like) the previous time through,
				//  be sure to clear the processing list before filling it with the active list.
				visorsToProcess.Clear();
				visorsToProcess.AddRange(activeVisors);
				
				// Process all active layers.
				for (int i = 0; i < numLayers; ++i)
				{
					// Use loop instead of List.Find to avoid allocations.
					for (int toProcIdx = 0; toProcIdx < visorsToProcess.Count; ++toProcIdx)
					{
						WwiseMediaIDVisor visor = visorsToProcess[toProcIdx];

						if (visor.MediaID == mediaIDs[i] && visor.AudioNodeID == audioNodeIDs[i])
						{
							// Update the audio position and go.
							visor.UpdateAudioPosition(audioTimes[i]);
							visor.Update();
							
							visorsToProcess.RemoveAt(toProcIdx);
							break;
						}
					}
				}
				
				// Any mapping that wasn't processed didn't have an associated MediaID running
				//  currently.  Their layers have stopped so we must remove them.
				for (int i = 0; i < visorsToProcess.Count; ++i)
				{
					WwiseMediaIDVisor visor = visorsToProcess[i];
					if (visor.HasStarted)
					{
						DeactivateMediaIDVisor(visor);
					}
				}
				
				// Clear out the processing list.
				visorsToProcess.Clear();
			}
			else
			{
				Debug.LogError("Received unexpected result " + result + " from AkSoundEngine.GetSourceMultiplePlayPositions().");
			}
		}
		
		#endregion
		#region IKoreographedPlayer Interface Methods
		
		/// <summary>
		/// Gets the current sample position of the audio file with name <paramref name="clipName"/>.
		/// </summary>
		/// <returns>The current sample position of the audio file with name
		/// <paramref name="clipName"/>.</returns>
		/// <param name="clipName">The name of the audio file to check.</param>
		public int GetSampleTimeForClip(string clipName)
		{
			int sampleTime = 0;

			for (int i = 0; i < activeVisors.Count; ++i)
			{
				WwiseMediaIDVisor visor = activeVisors[i];
				if (visor.AudioName == clipName)
				{
					sampleTime = visor.SampleTime;
					break;
				}
			}
			
			return sampleTime;
		}
		
		/// <summary>
		/// Gets the total sample time for audio file with name <paramref name="clipName"/>.  
		/// This total time is not necessarily the same at runtime as it was at edit time.
		/// </summary>
		/// <returns>The total sample time for the audio file with name <paramref name="clipName"/>.</returns>
		/// <param name="clipName">The name of the audio file to check.</param>
		public int GetTotalSampleTimeForClip(string clipName)
		{
			int totalTime = 0;

			for (int i = 0; i < activeVisors.Count; ++i)
			{
				WwiseMediaIDVisor visor = activeVisors[i];
				if (visor.AudioName == clipName)
				{
					totalTime = visor.SampleDuration;
					break;
				}
			}
			
			return totalTime;
		}
		
		/// <summary>
		/// Determines whether the audio file with name <paramref name="clipName"/> is playing.
		/// </summary>
		/// <returns><c>true</c>, if the audio file with name <paramref name="clipName"/> is
		/// playing,<c>false</c> otherwise.</returns>
		/// <param name="clipName">The name of the audio file to check.</param>
		public bool GetIsPlaying(string clipName)
		{
			bool bPlaying = false;

			for (int i = 0; i < activeVisors.Count; ++i)
			{
				WwiseMediaIDVisor visor = activeVisors[i];
				if (visor.AudioName == clipName)
				{
					bPlaying = visor.IsPlaying;
					break;
				}
			}
			
			return bPlaying;
		}
		
		/// <summary>
		/// Gets the pitch.  The <paramref name="clipName"/> parameter is ignored.
		/// </summary>
		/// <returns>The pitch of the audio.</returns>
		/// <param name="clipName">The name of the audio file to check.  Currently ignored.</param>
		public float GetPitch(string clipName)
		{
			// Currently we don't have a way to get the pitch (or playback speed).
			return 1f;
		}
		
		/// <summary>
		/// Gets the name of the current audio file.
		/// </summary>
		/// <returns>The name of the earliest-played audio file in the music hierarchy,
		/// if any.</returns>
		public string GetCurrentClipName()
		{
			string clipName = string.Empty;
			
			if (activeVisors.Count > 0)
			{
				clipName = activeVisors[0].AudioName;
			}
			
			return clipName;
		}
		
		#endregion
	}
}
