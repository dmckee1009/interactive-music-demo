﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System;

//This is mainly for animation events.
public class playWwiseMusicCallback : MonoBehaviour {

    //public string sound = null;
    public GameObject GameObject;
    //public string VoiceOver;
    public AK.Wwise.Bank MusicBank;
    public AK.Wwise.Bank SFXBank;
    //public string music;
    public AK.Wwise.Event MyEvent;





    public void Start()
    {
        MusicBank.Load();
        SFXBank.Load();
        //AkSoundEngine.LoadBank(VoiceOver, )
        //AkSoundEngine.PostEvent(music, GameObject);
        MyEvent.Post(GameObject, (uint)AkCallbackType.AK_MusicSyncBeat,
            null, this);

    }


    public AK.Wwise.Event sound1;
    public AK.Wwise.Event sound2;
    public AK.Wwise.Event sound3;
    public AK.Wwise.Event sound4;

    //This is an animation event
    public void triggerSound1()
    {
        //sound1.Post(GameObject);


    }


    object in_cookie;
    AkCallbackType in_type;
    object in_callbackinfo;
    
    void playSound1 (object in_cookie, AkCallbackType in_type, object in_callbackInfo)
    {
        AkMusicSyncCallbackInfo musicSyncInfo = in_callbackInfo as AkMusicSyncCallbackInfo;
        sound1.Post(GameObject);  
    }

   /* //Check to see if a button was pushed and do s
    void Update()
    {
        if (ReInput.players.GetPlayer(0).GetButtonDown("go"))
        {
            playSound1(in_cookie, in_type, in_callbackInfo);
            Debug.Log("BEEP!");
        }
    }*/
}
