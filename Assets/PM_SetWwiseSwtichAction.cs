﻿// custom action example by M dot Strange
// goes with this video tutorial
//https://www.youtube.com/watch?v=U0zm2EVqDBU
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Wwise Audio")]
    [Tooltip("Sets the state ofa Wwise object to a specificed switch.")]

    // the class must match the name of the action
    // if the action is named missleAction then that should be the name of the class
    public class PM_SetWwiseSwitchAction : FsmStateAction
    {
        [RequiredField]

        // add the name of your script inside of typeof("yourScriptName"))
        // if your script name is homingMissile
        // then it should look like below
        // [CheckForComponent(typeof(homingMissile))]

        // we are using the exampleScript so we use the following
        [CheckForComponent(typeof(SetWwiseSwitch))]

        // this is the game object the script is on
        public FsmOwnerDefault gameObject;

        // add the variables you want in your action
        // feel free to use the types I made or delete/replace... whatever

        public FsmString stateGroupName;
        public FsmString stateName;


        // you can usually leave this alone
        public FsmBool everyFrame;

        // you are making a custom variable with the scripts type
        //name it anything you want- I just used "theScript"
        SetWwiseSwitch theScript;


        public override void Reset()
        {
            //its good practice to set your var to null at start
            gameObject = null;
            stateGroupName = null;
            stateName = null;
            //setting V3's to null looks like this
            //targetPosition = new Vector3(0, 0, 0);
            everyFrame = true;


        }

        public override void OnEnter()
        {
            var go = Fsm.GetOwnerDefaultTarget(gameObject);

            // you are grabbing the script from the game object and storing it in your custom variable type
            //this is the var you made in line 40
            // make sure the names match
            theScript = go.GetComponent<SetWwiseSwitch>();


            if (!everyFrame.Value)
            {
                DoTheMagic();
                Finish();
            }

        }

        public override void OnUpdate()
        {
            if (everyFrame.Value)
            {
                DoTheMagic();
            }
        }

        //Name your method here
        void DoTheMagic()
        {
            var go = Fsm.GetOwnerDefaultTarget(gameObject);
            if (go == null)
            {
                return;
            }

            // THIS is where the magic happens!

            //Lets grab the health var from our script and use it to set our
            // playmaker playerHealth variable

            //stateName.Value = theScript.stateName;
            //switchName.Value = theScript.switchName;

            //lets do the same with our game object variable

            //targetGameobject.Value = theScript.myObject;

            //Now lets do the reverse and have Playmaker set the scripts
            //variable using the targetPosition variable

            theScript.stateGroupName = stateGroupName.Value;
            theScript.stateName = stateName.Value;

            //Note! Playmaker var's need .Value after them or they won't work in some cases



        }

    }
}