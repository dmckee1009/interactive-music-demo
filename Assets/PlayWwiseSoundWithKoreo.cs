﻿//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	    _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
// 
using UnityEngine;
using HutongGames.PlayMaker;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;
using SonicBloom.Koreo.Players.Wwise;

namespace Nonatomic.PlayMaker.Actions
{
    [ActionCategory("Wwise Audio")]
    [Tooltip("Plays a Wwise Sound with Koreography")]
    public class PlayWwiseSoundWithKoreo : FsmStateAction
    {
        public FsmOwnerDefault gameObject;

        [RequiredField]
        [Tooltip("This is the name of the soundbank. You do not need to include the .bnk file extension")]
        [Title("SoundBank")]
        public FsmString soundBank;

        [RequiredField]
        [Title("EventName")]
        public FsmString eventName;

        [Title("Delay")]
        public FsmFloat delay;

        [Title("One Shot")]
        public FsmBool oneShot;
        private bool triggered;



        private float remainingDelay;
        private GameObject go;

        WwiseSuperVisor superVisor;


        public override void Reset()
        {
            gameObject = null;
        }

        public override void OnEnter()
        {
            remainingDelay = delay.Value;

            go = Fsm.GetOwnerDefaultTarget(gameObject);
            superVisor = go.GetComponent<WwiseSuperVisor>();

            //soundBank is the name of a Wwise soundBank to target. SoundBanks have a .bnk file extension
            if (!soundBank.Value.Contains(".bnk"))
            {
                soundBank.Value += ".bnk";
            }

            //Collider is required
            if (go.GetComponent<Collider>() == null)
            {
                Debug.LogWarning("To play Wwise sound requires a collider on the target GameObject. Adding a default BoxCollider, but recommend you add your own");
                go.AddComponent<BoxCollider>();
            }

            if (delay.Value == 0)
            {
                PlaySound();
            }
        }

        public override void OnUpdate()
        {
            if (remainingDelay <= 0)
                return;

            remainingDelay -= Time.deltaTime;

            if (remainingDelay <= 0)
            {
                PlaySound();
            }
        }

        private void PlaySound()
        {
            if (oneShot.Value && triggered)
                return;

            triggered = true;

            uint bankID;
            AkSoundEngine.LoadBank(soundBank.Value, AkSoundEngine.AK_DEFAULT_POOL_ID, out bankID);
            AkSoundEngine.PostEvent(eventName.Value, go, (uint)(AkCallbackType.AK_Duration |
            AkCallbackType.AK_EnableGetSourcePlayPosition | AkCallbackType.AK_EndOfEvent),
            superVisor.HandleAKCallback, null);
        }
    }
}