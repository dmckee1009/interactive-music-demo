/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AREA1 = 629923843U;
        static const AkUniqueID AREA2 = 629923840U;
        static const AkUniqueID AREAOFFLINE = 2427688735U;
        static const AkUniqueID AREOFFLINEX = 4072995806U;
        static const AkUniqueID BACK = 1559875400U;
        static const AkUniqueID BEATSYNC = 1067713692U;
        static const AkUniqueID BGDRUMS = 2609767683U;
        static const AkUniqueID BGDRUMS2 = 3814687627U;
        static const AkUniqueID BGSTART = 146821428U;
        static const AkUniqueID BGSTART_ALT = 902446744U;
        static const AkUniqueID CHANGE = 522632697U;
        static const AkUniqueID CHORD1 = 2614957254U;
        static const AkUniqueID CHORD2 = 2614957253U;
        static const AkUniqueID CHORD3 = 2614957252U;
        static const AkUniqueID CHORD4 = 2614957251U;
        static const AkUniqueID CHORDOFF = 3903699212U;
        static const AkUniqueID DRKICK = 652878897U;
        static const AkUniqueID DRSHAKER = 746480971U;
        static const AkUniqueID DRSHAKER2 = 1442411811U;
        static const AkUniqueID DRSNARE = 2048836982U;
        static const AkUniqueID EXIT = 26822469U;
        static const AkUniqueID FUTUREBINFO = 2341146698U;
        static const AkUniqueID GCHORD1 = 2245437403U;
        static const AkUniqueID GCHORD2 = 2245437400U;
        static const AkUniqueID GCHORD3 = 2245437401U;
        static const AkUniqueID GCHORD4 = 2245437406U;
        static const AkUniqueID GCHORDOFF = 401202141U;
        static const AkUniqueID INTENSEINFO = 1796835497U;
        static const AkUniqueID MOVETOCHOICE = 3740729624U;
        static const AkUniqueID NOTICE = 2981522829U;
        static const AkUniqueID NOTICE2 = 1328444613U;
        static const AkUniqueID PAUSE = 3092587493U;
        static const AkUniqueID PAUSE_ALL = 3864097025U;
        static const AkUniqueID PLAY_A2_INSTRUCTIONS = 2515370705U;
        static const AkUniqueID PLAYMUSIC = 417627684U;
        static const AkUniqueID PRESSSTART = 3540125970U;
        static const AkUniqueID RESTART = 1203400786U;
        static const AkUniqueID RESUME = 953277036U;
        static const AkUniqueID RESUME_ALL = 3679762312U;
        static const AkUniqueID RETRO_SONG = 3158443289U;
        static const AkUniqueID SELECTCHOICE = 1496243188U;
        static const AkUniqueID START = 1281810935U;
        static const AkUniqueID STOPALL = 3086540886U;
        static const AkUniqueID STOPAUDIO = 2509093697U;
        static const AkUniqueID TITLE = 3705726509U;
        static const AkUniqueID TITLETIP = 1203933600U;
        static const AkUniqueID TRANSITION = 1865857008U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace BCHORDS
        {
            static const AkUniqueID GROUP = 2029505468U;

            namespace STATE
            {
                static const AkUniqueID BCHORD1 = 2029505534U;
                static const AkUniqueID BCHORD2 = 2029505533U;
                static const AkUniqueID BCHORD3 = 2029505532U;
                static const AkUniqueID BCHORD4 = 2029505531U;
            } // namespace STATE
        } // namespace BCHORDS

        namespace BGDRUMS
        {
            static const AkUniqueID GROUP = 2609767683U;

            namespace STATE
            {
                static const AkUniqueID BUILDUP = 2149848048U;
                static const AkUniqueID PART1 = 3177314147U;
                static const AkUniqueID PART2 = 3177314144U;
                static const AkUniqueID PATTERNS = 4163340556U;
            } // namespace STATE
        } // namespace BGDRUMS

        namespace DRPATTERNS
        {
            static const AkUniqueID GROUP = 727293446U;

            namespace STATE
            {
                static const AkUniqueID BEAT1 = 4259886514U;
                static const AkUniqueID BEAT1HATSONLY = 1083138586U;
                static const AkUniqueID BEAT1NOHATS = 1337771601U;
                static const AkUniqueID SNAREROLL = 2690248593U;
            } // namespace STATE
        } // namespace DRPATTERNS

        namespace FUTUREBASS_GCHORDACTIVE
        {
            static const AkUniqueID GROUP = 2067088169U;

            namespace STATE
            {
                static const AkUniqueID GCHORD1 = 2245437403U;
                static const AkUniqueID GCHORD2 = 2245437400U;
                static const AkUniqueID GCHORD3 = 2245437401U;
                static const AkUniqueID GCHORD4 = 2245437406U;
                static const AkUniqueID OFF = 930712164U;
            } // namespace STATE
        } // namespace FUTUREBASS_GCHORDACTIVE

        namespace FUTUREBASSBG
        {
            static const AkUniqueID GROUP = 633794348U;

            namespace STATE
            {
                static const AkUniqueID A2 = 1886858600U;
                static const AkUniqueID A3 = 1886858601U;
                static const AkUniqueID A4 = 1886858606U;
                static const AkUniqueID AB1 = 1134309145U;
            } // namespace STATE
        } // namespace FUTUREBASSBG

        namespace MELCHORDS
        {
            static const AkUniqueID GROUP = 3893972548U;

            namespace STATE
            {
                static const AkUniqueID MELCHORD1 = 3893972486U;
                static const AkUniqueID MELCHORD2 = 3893972485U;
                static const AkUniqueID MELCHORD3 = 3893972484U;
                static const AkUniqueID MELCHORD4 = 3893972483U;
            } // namespace STATE
        } // namespace MELCHORDS

        namespace MELODY
        {
            static const AkUniqueID GROUP = 1040548709U;

            namespace STATE
            {
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID SLOW = 787604482U;
            } // namespace STATE
        } // namespace MELODY

        namespace RETRO
        {
            static const AkUniqueID GROUP = 3496907731U;

            namespace STATE
            {
                static const AkUniqueID END = 529726532U;
                static const AkUniqueID INTRO = 1125500713U;
                static const AkUniqueID PART1 = 3177314147U;
                static const AkUniqueID PART2 = 3177314144U;
                static const AkUniqueID PART3 = 3177314145U;
            } // namespace STATE
        } // namespace RETRO

        namespace START_SCREEN
        {
            static const AkUniqueID GROUP = 1705842598U;

            namespace STATE
            {
                static const AkUniqueID INTRO = 1125500713U;
                static const AkUniqueID LOOP = 691006007U;
                static const AkUniqueID OUTRO = 4184794294U;
            } // namespace STATE
        } // namespace START_SCREEN

    } // namespace STATES

    namespace SWITCHES
    {
        namespace FUTUREBASS
        {
            static const AkUniqueID GROUP = 3894258857U;

            namespace SWITCH
            {
                static const AkUniqueID CHORD1ARP = 4118912561U;
                static const AkUniqueID CHORD2ARP = 2268328584U;
                static const AkUniqueID CHORD3ARP = 340983183U;
                static const AkUniqueID CHORD4ARP = 2878010942U;
            } // namespace SWITCH
        } // namespace FUTUREBASS

        namespace RETRO_SPEED
        {
            static const AkUniqueID GROUP = 1026968247U;

            namespace SWITCH
            {
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID MEDIUM = 2849147824U;
                static const AkUniqueID SLOW = 787604482U;
            } // namespace SWITCH
        } // namespace RETRO_SPEED

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AUDIOOVOLUME = 2297955818U;
        static const AkUniqueID GCHORDACTIVE = 1766052116U;
        static const AkUniqueID INTENSITY = 2470328564U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID BGDRUMS = 2609767683U;
        static const AkUniqueID CHORD1 = 2614957254U;
        static const AkUniqueID CHORD2 = 2614957253U;
        static const AkUniqueID CHORD3 = 2614957252U;
        static const AkUniqueID CHORD4 = 2614957251U;
        static const AkUniqueID DRKICK = 652878897U;
        static const AkUniqueID DRSHAKER = 746480971U;
        static const AkUniqueID DRSHAKER2 = 1442411811U;
        static const AkUniqueID DRSNARE = 2048836982U;
        static const AkUniqueID GCHORD1 = 2245437403U;
        static const AkUniqueID GCHORD2 = 2245437400U;
        static const AkUniqueID GCHORD3 = 2245437401U;
        static const AkUniqueID GCHORD4 = 2245437406U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID DEMOSONGS = 2018015686U;
        static const AkUniqueID FUTUREBASS = 3894258857U;
        static const AkUniqueID INTENSE = 4223512837U;
        static const AkUniqueID RETRO = 3496907731U;
        static const AkUniqueID START_SCREEN = 1705842598U;
        static const AkUniqueID UISFX = 1655394148U;
        static const AkUniqueID VOICEOVER = 4041657371U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID BACKGROUND = 3650723969U;
        static const AkUniqueID CHORDS = 2614957188U;
        static const AkUniqueID DRUMS = 2726606664U;
        static const AkUniqueID FUTUREBASS = 3894258857U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID VOICE = 3170124113U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
