﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;


namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Wwise Audio")]
    [Tooltip("Switches the state of a Wwise object.")]

    public class SetWwiseState : FsmStateAction
    {

        public FsmString stateGroupName;
        public FsmString stateName;
        string nameState;
        string groupName;



        public override void Reset()
        {
            stateGroupName = null;
            stateName = null;
            Debug.Log(stateGroupName);
            Debug.Log(stateName);
        }
        public override void OnEnter()
        {
            nameState = stateName.Value;
            groupName = stateGroupName.Value;
            AkSoundEngine.SetState(groupName, nameState);
        }


    }

}

